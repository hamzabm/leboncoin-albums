package com.leboncoin.lbtechtestapp


import com.leboncoin.domain.common.Mapper
import com.leboncoin.domain.entities.AlbumsSourcesEntity
import com.leboncoin.domain.entities.DataEntity
import com.leboncoin.domain.repository.AlbumsRepository
import com.leboncoin.domain.usecases.GetAlbumsUseCase
import com.leboncoin.lbtechtestapp.entities.AlbumsSources
import com.leboncoin.lbtechtestapp.entities.Data
import com.leboncoin.lbtechtestapp.mappers.AlbumsEntityMapper
import com.leboncoin.lbtechtestapp.ui.main.MainViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Before
import org.junit.Test

import org.mockito.Mock
import org.mockito.MockitoAnnotations

class AlbumsViewModelTest {




    @Mock
    lateinit var viewModel: MainViewModel

    @Mock
    lateinit var repositories: AlbumsRepository

    lateinit var newsUseCase: GetAlbumsUseCase

    lateinit var mapper:  Mapper<DataEntity<AlbumsSourcesEntity>, Data<AlbumsSources>>

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    GlobalScope.launch {
        mapper = AlbumsEntityMapper()
        newsUseCase = GetAlbumsUseCase(coroutineContext, repositories)
        viewModel = MainViewModel(newsUseCase, mapper)
    }
    }

    @Test
    fun fetchNewsForEmptyDataFromUseCase() {
        setDataForRepository(AlbumsSourcesEntity())

        viewModel.fetchAlbums()

        val viewModelValue = viewModel.getAlbumsLiveData().value
        assert(viewModelValue == null)
    }

    @Test
    fun fetchNewsForDataFromUseCase() {
        setDataForRepository(PresentationUtil.createTestDataForNewsEntity(LIST))

        viewModel.fetchAlbums()

        val viewModelValue = viewModel.getAlbumsLiveData().value
        viewModelValue?.let {
            when(viewModelValue){
                is Data.ERROR -> {
                    //Error handling
                }
                is Data.LOADING -> {
                    //Progress
                }
                is Data.SUCCESS -> {
//                    it?.data?.albums?.let {
//                        it1 ->
//                        assert(it1.size == LIST.size)
//                    }
                }
            }
        }


    }

    private fun setDataForRepository(entity: AlbumsSourcesEntity?) {
        GlobalScope.launch {
         //   `when`(repositories.getAlbums()).thenReturn(ReceiveChannel.just(entity))
        }
    }

    companion object {
        val LIST =PresentationUtil.getDummyList()
    }
}