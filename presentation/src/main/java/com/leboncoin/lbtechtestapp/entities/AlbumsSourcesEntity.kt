package com.leboncoin.lbtechtestapp.entities

import com.leboncoin.domain.entities.AlbumsEntity

data class AlbumsSourcesEntity(
        var status: String? = null,
        var albums: List<AlbumsEntity> = emptyList()
)