package com.leboncoin.lbtechtestapp.entities

data class Album( var albumId: Int,
                  var id: Int,
                  var thumbnailUrl: String,
                  var title: String,
                  var url: String)