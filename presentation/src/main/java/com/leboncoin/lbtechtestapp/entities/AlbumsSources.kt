package com.leboncoin.lbtechtestapp.entities

data class AlbumsSources(
        var status: String? = null,
        var albums: List<Album> = emptyList()
)