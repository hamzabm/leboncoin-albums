package com.leboncoin.lbtechtestapp

import android.app.Application
import com.leboncoin.lbtechtestapp.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin


class App : Application() {

    override fun onCreate() {
        super.onCreate()
        loadKoin()
    }

    private fun loadKoin() {
            startKoin {
                androidLogger()
                        androidContext(this@App)
                        modules(listOf(mNetworkModules,
                    mViewModels,
                    mRepositoryModules,
                    mUseCaseModules,
                    mLocalModules))
            }

    }
}