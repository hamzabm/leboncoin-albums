package com.leboncoin.lbtechtestapp.mappers

import com.leboncoin.domain.common.Mapper
import com.leboncoin.domain.entities.AlbumsEntity
import com.leboncoin.domain.entities.AlbumsSourcesEntity
import com.leboncoin.domain.entities.DataEntity
import com.leboncoin.lbtechtestapp.entities.Album
import com.leboncoin.lbtechtestapp.entities.AlbumsSources
import com.leboncoin.lbtechtestapp.entities.Data
import com.leboncoin.lbtechtestapp.entities.Error


class AlbumsEntityMapper : Mapper<DataEntity<AlbumsSourcesEntity>, Data<AlbumsSources>>() {

    override fun mapFrom(data: DataEntity<AlbumsSourcesEntity>): Data<AlbumsSources> {
        when (data) {
            is DataEntity.SUCCESS -> return@mapFrom Data.SUCCESS(data.data?.let { mapSourcesToPresentation(it) })
            is DataEntity.ERROR -> return@mapFrom  Data.ERROR(error = Error(data.error.message))
            is DataEntity.LOADING -> return@mapFrom  Data.LOADING()
        }
    }

    private fun mapSourcesToPresentation(sources: AlbumsSourcesEntity)
            : AlbumsSources = AlbumsSources(status = sources?.status,
    albums = mapListAlbumsToPresentation(sources?.albums))


    private fun mapListAlbumsToPresentation(albums: List<AlbumsEntity>?)
            : List<Album> = albums?.map { mapAlbumToPresentation(it) }
            ?: emptyList()

    private fun mapAlbumToPresentation(response: AlbumsEntity): Album = Album(
            id = response.id,
            thumbnailUrl = response.thumbnailUrl,
            albumId = response.albumId,
            url = response.url,
            title = response.title
    )

}