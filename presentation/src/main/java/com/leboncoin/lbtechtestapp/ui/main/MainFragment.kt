package com.leboncoin.lbtechtestapp.ui.main

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
import com.leboncoin.lbtechtestapp.R
import com.leboncoin.lbtechtestapp.entities.Data
import com.rakshitjain.presentation.Albums.AlbumsListAdapter
import kotlinx.android.synthetic.main.main_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private val AlbumsListModel: MainViewModel by viewModel()
    private lateinit var listAdapter: AlbumsListAdapter
    private lateinit var gridLayoutManager: GridLayoutManager


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listAdapter = AlbumsListAdapter()
        gridLayoutManager=GridLayoutManager(requireContext(), 1)
        if (resources.getBoolean(R.bool.is_tablet)) {
            if (resources.getBoolean(R.bool.is_port)) {
                gridLayoutManager.spanCount = 3
            } else {
                gridLayoutManager.spanCount = 4
            }
        }else{
            if (!resources.getBoolean(R.bool.is_port)) {
                gridLayoutManager.spanCount = 2

            }
        }
        recycler_view_albums.layoutManager = gridLayoutManager
        recycler_view_albums.adapter = listAdapter
        AlbumsListModel.getAlbumsLiveData().observe(this, {
            when (it) {
                is Data.ERROR -> {
                    //Error handling
                }
                is Data.LOADING -> {
                    //Progress
                }
                is Data.SUCCESS -> {
                    it.data?.albums?.let { it1 -> listAdapter.updateList(it1) }
                }
            }
        })
        AlbumsListModel.fetchAlbums()

    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        if (resources.getBoolean(R.bool.is_tablet)) {
            if (resources.getBoolean(R.bool.is_port)) {
                gridLayoutManager.spanCount = 3
            } else {
                gridLayoutManager.spanCount = 4
            }
        }else{
            if (!resources.getBoolean(R.bool.is_port)) {
                gridLayoutManager.spanCount = 2

            }
        }
    }


}