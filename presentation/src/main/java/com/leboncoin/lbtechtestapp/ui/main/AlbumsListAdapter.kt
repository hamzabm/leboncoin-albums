package com.rakshitjain.presentation.Albums

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.leboncoin.lbtechtestapp.R
import com.leboncoin.lbtechtestapp.di.PicassoBuilder
import com.leboncoin.lbtechtestapp.entities.Album
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.album_item.view.*

class AlbumsListAdapter : RecyclerView.Adapter<AlbumsListAdapter.AlbumsViewHolder>() {

    var albums = mutableListOf<Album>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.album_item, parent, false)
        return AlbumsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return albums.size
    }

    override fun onBindViewHolder(holder: AlbumsViewHolder, position: Int) {
        holder.bind(albums[position])
    }

    class AlbumsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(alb: Album) {
            with(itemView) {
                title.text=alb.title
                url.text = String.format(context.getString(R.string.url),alb.url)
                idObj.text=String.format(context.getString(R.string.obj_id),alb.id.toString())
                albumId.text=String.format(context.getString(R.string.album_id),alb.albumId.toString())
                Picasso.get().load(alb.thumbnailUrl).into(thumb)
            }
        }
    }

    fun updateList(list: List<Album>) {
        if (list.isNotEmpty()) {
            albums.clear()
            albums.addAll(list)
            notifyDataSetChanged()
        }
    }
}