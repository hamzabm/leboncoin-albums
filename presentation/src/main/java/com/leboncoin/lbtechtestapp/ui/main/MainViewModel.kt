package com.leboncoin.lbtechtestapp.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.leboncoin.domain.common.Mapper
import com.leboncoin.domain.entities.AlbumsSourcesEntity
import com.leboncoin.domain.entities.DataEntity
import com.leboncoin.domain.usecases.GetAlbumsUseCase
import com.leboncoin.lbtechtestapp.common.BaseViewModel
import com.leboncoin.lbtechtestapp.entities.AlbumsSources
import com.leboncoin.lbtechtestapp.entities.Data
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.consumeEach

class MainViewModel(private val getAlbumsUseCase: GetAlbumsUseCase,
                    private val mapper: Mapper<DataEntity<AlbumsSourcesEntity>, Data<AlbumsSources>>
) : BaseViewModel() {
    var mAlbums = MutableLiveData<Data<AlbumsSources>>()

    fun fetchAlbums() {
       launch {
            val Albums = getAlbumsUseCase.getAlbums()
            Albums.consumeEach { response ->
                val mappedResponse = mapper.mapFrom(response)

                //Switching the context to main
                withContext(Dispatchers.Main) {
                    mAlbums.postValue(mappedResponse)
                }
            }
        }
    }

    fun getAlbumsLiveData() = mAlbums
}