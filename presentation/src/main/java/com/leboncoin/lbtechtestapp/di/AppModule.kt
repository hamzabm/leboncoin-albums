package com.leboncoin.lbtechtestapp.di

import androidx.room.Room
import com.leboncoin.data.api.RemoteAlbumsApi
import com.leboncoin.data.db.AlbumsDao
import com.leboncoin.data.db.AlbumsDatabase
import com.leboncoin.data.entities.AlbumsDataEntityMapper
import com.leboncoin.data.entities.AlbumsEntityDataMapper
import com.leboncoin.data.repository.AlbumsCacheImpl
import com.leboncoin.data.repository.AlbumsRemoteImpl
import com.leboncoin.data.repository.AlbumsRepositoryImpl
import com.leboncoin.domain.repository.AlbumsRepository
import com.leboncoin.domain.usecases.GetAlbumsUseCase
import com.leboncoin.lbtechtestapp.mappers.AlbumsEntityMapper
import com.leboncoin.lbtechtestapp.ui.main.MainViewModel
import kotlinx.coroutines.Dispatchers
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit



val mRepositoryModules = module {
    single(named( "remote")) { AlbumsRemoteImpl(api = get(named(API)))}
    single(named( "local")) {
        AlbumsCacheImpl(database = get(named(DATABASE)), entityToDataMapper = AlbumsEntityDataMapper(),
            dataToEntityMapper = AlbumsDataEntityMapper())
    }
    single { AlbumsRepositoryImpl(remote = get(named("remote")), cache = get(named("local"))) as AlbumsRepository }
}

val mUseCaseModules = module {
    factory(named(GET_ALBUMS_USECASE)) { GetAlbumsUseCase(coroutineContext = Dispatchers.Default, repositories = get()) }
}

val mNetworkModules = module {
    single(named( RETROFIT_INSTANCE)) { createNetworkClient(BASE_URL) }
    single(named( API)) { (get(named(RETROFIT_INSTANCE)) as Retrofit).create(RemoteAlbumsApi::class.java) }
}


val mLocalModules = module {
    single(named(DATABASE)) { Room.databaseBuilder(androidApplication(), AlbumsDatabase::class.java, "Albums_albums").build() }

}

val mViewModels = module {
    viewModel {
        MainViewModel(getAlbumsUseCase = get(named(GET_ALBUMS_USECASE)), mapper = AlbumsEntityMapper())
    }
}

private const val RETROFIT_INSTANCE = "Retrofit"
private const val API = "Api"
private const val GET_ALBUMS_USECASE = "getAlbumsUseCase"
private const val DATABASE = "database"

private const val BASE_URL = "https://static.leboncoin.fr"