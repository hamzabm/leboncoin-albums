package com.leboncoin.domain.repository

import com.leboncoin.domain.entities.AlbumsSourcesEntity
import com.leboncoin.domain.entities.DataEntity
import kotlinx.coroutines.channels.ReceiveChannel

interface AlbumsRepository {

    suspend fun getAlbums(): ReceiveChannel<DataEntity<AlbumsSourcesEntity>>
    suspend fun getLocalAlbums(): ReceiveChannel<DataEntity<AlbumsSourcesEntity>>
    suspend fun getRemoteAlbums(): ReceiveChannel<DataEntity<AlbumsSourcesEntity>>

}