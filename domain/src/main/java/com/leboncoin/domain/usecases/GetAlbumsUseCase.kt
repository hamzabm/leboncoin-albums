package com.leboncoin.domain.usecases

import com.leboncoin.domain.common.BaseJobUseCase
import com.leboncoin.domain.entities.AlbumsSourcesEntity
import com.leboncoin.domain.entities.DataEntity
import com.leboncoin.domain.repository.AlbumsRepository
import kotlinx.coroutines.channels.ReceiveChannel
import kotlin.coroutines.CoroutineContext

/**
 * It will first get albums from the local database and also update it with the latest
 * albums from remote
 */
class GetAlbumsUseCase(private val coroutineContext: CoroutineContext,
                       private val repositories: AlbumsRepository
) : BaseJobUseCase<AlbumsSourcesEntity>(coroutineContext) {

    override suspend fun getDataChannel(data: Map<String, Any>?): ReceiveChannel<DataEntity<AlbumsSourcesEntity>> {
        return repositories.getAlbums()
    }

    override suspend fun sendToPresentation(data: DataEntity<AlbumsSourcesEntity>): DataEntity<AlbumsSourcesEntity> {
        return data
    }

    suspend fun getAlbums(): ReceiveChannel<DataEntity<AlbumsSourcesEntity>> {
        val data = HashMap<String, String>()
        return produce(data)
    }
}