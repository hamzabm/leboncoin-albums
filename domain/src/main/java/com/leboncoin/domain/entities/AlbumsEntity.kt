package com.leboncoin.domain.entities

data class AlbumsEntity(
        var albumId: Int,
        var id: Int,
        var thumbnailUrl: String,
        var title: String,
        var url: String)