package com.leboncoin.domain.entities

data class AlbumsSourcesEntity(
        var status: String? = null,
        var albums: List<AlbumsEntity> = emptyList()
)