package com.leboncoin.data.repository

import androidx.room.Room
import com.leboncoin.data.TestUtil
import com.leboncoin.data.TestUtil.createTestDataEntitySUCCESS
import com.leboncoin.data.TestUtil.createTestDataForAlbumsEntity
import com.leboncoin.data.db.AlbumsDao
import com.leboncoin.data.db.AlbumsDatabase
import com.leboncoin.data.entities.AlbumData
import com.leboncoin.data.entities.AlbumsDataEntityMapper
import com.leboncoin.data.entities.AlbumsEntityDataMapper
import com.leboncoin.domain.common.Mapper
import com.leboncoin.domain.entities.AlbumsSourcesEntity
import com.leboncoin.domain.entities.DataEntity
import io.reactivex.Flowable
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations

class AlbumsRepoImpUnitTest:KoinTest {
    private var entityToDataMapper = AlbumsEntityDataMapper()
    private var dataToEntityMapper = AlbumsDataEntityMapper()
    private lateinit var local: AlbumsCacheImpl

    @Mock
    private lateinit var database: AlbumsDatabase
    /**
     * In-Memory Room Database definition
     */
    val roomTestModule = module {
        single {
            // In-Memory database config
            Room.inMemoryDatabaseBuilder(get(), AlbumsDatabase::class.java)
                .allowMainThreadQueries()
                .build()
        }
    }
    @Before
    fun setUp() {
        loadKoinModules(roomTestModule)

        MockitoAnnotations.initMocks(this)
        val dao = mock(AlbumsDao::class.java)
        `when`(database.getAlbumsDao()).thenReturn(dao)
        local = AlbumsCacheImpl(database, entityToDataMapper, dataToEntityMapper)
    }

    @Test
    fun getListFromDatabase() = runBlocking {
        setAlbumsListInDatabase()
        local.getAlbums().consumeEach {
            when (it) {
                is DataEntity.ERROR -> {
                    //Error handling
                }
                is DataEntity.LOADING -> {
                    //Progress
                }
                is DataEntity.SUCCESS -> {
                    it.data?.albums?.let { it1 ->
                        assert(it1[0].id==AlbumsSourcesEntity(albums = LIST_ENTITY).albums[0].id)
                    }
                }
            }

        }
    }

    private fun setAlbumsListInDatabase() {
        local.saveAlbums(LIST_DATA)
    }

    companion object {
        val LIST_DATA = createTestDataEntitySUCCESS()
        val LIST_ENTITY = TestUtil.getDummyList()
    }
}