package com.leboncoin.data

import com.leboncoin.data.entities.AlbumData
import com.leboncoin.domain.entities.AlbumsEntity
import com.leboncoin.domain.entities.AlbumsSourcesEntity
import com.leboncoin.domain.entities.DataEntity

object TestUtil{

    fun createTestDataForAlbums():List<AlbumData>{
        val data = AlbumData(id = 12312,title = "title",thumbnailUrl = "thumb",albumId = 1,url = "url")
        return listOf<AlbumData>(data)
    }

    fun createTestDataEntitySUCCESS():DataEntity<AlbumsSourcesEntity>{
        return DataEntity.SUCCESS(createTestDataForAlbumsEntity(data = getDummyList()))
              //  is DataEntity.ERROR -> return  DataEntity.ERROR(error = Error(data.error.message))
            //    is DataEntity.LOADING -> return  DataEntity.LOADING()
    }

    fun createTestDataForAlbumsEntity(data: List<AlbumsEntity>): AlbumsSourcesEntity{
        return AlbumsSourcesEntity(status = "200",albums = data)
    }

    fun getDummyList():List<AlbumsEntity>{
        val data = AlbumsEntity(id = 12312,title = "title",thumbnailUrl = "thumb",albumId = 1,url = "url")
        return listOf(data)
    }

    fun getDummyListData():List<AlbumData>{
        val data = AlbumData(id = 12312,title = "title",thumbnailUrl = "thumb",albumId = 1,url = "url")
        return listOf(data)
    }
}