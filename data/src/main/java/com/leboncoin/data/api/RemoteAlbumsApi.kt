package com.leboncoin.data.api

import com.leboncoin.data.entities.AlbumData
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

interface RemoteAlbumsApi {

    @GET("/img/shared/technical-test.json")
    fun getAlbums(): Deferred<List<AlbumData>>

}