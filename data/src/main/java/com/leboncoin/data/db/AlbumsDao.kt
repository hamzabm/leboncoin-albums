package com.leboncoin.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.leboncoin.data.entities.AlbumData
import io.reactivex.Flowable

@Dao
interface AlbumsDao{

    @Query("Select * from albums")
    fun getAllAlbums(): Flowable<List<AlbumData>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAllAlbums(albums: List<AlbumData>)

    @Query("DELETE FROM albums")
    fun clear()

}