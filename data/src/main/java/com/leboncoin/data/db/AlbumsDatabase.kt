package com.leboncoin.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.leboncoin.data.entities.AlbumData

@Database(entities = arrayOf(AlbumData::class), version = 1)
abstract class AlbumsDatabase : RoomDatabase() {
    abstract fun getAlbumsDao(): AlbumsDao
}