package com.leboncoin.data.repository

import com.leboncoin.domain.entities.AlbumsSourcesEntity
import com.leboncoin.domain.entities.DataEntity
import kotlinx.coroutines.channels.ReceiveChannel


interface AlbumsDataStore{
    suspend fun getAlbums(): ReceiveChannel<DataEntity<AlbumsSourcesEntity>>
}