package com.leboncoin.data.repository

import com.leboncoin.data.db.AlbumsDao
import com.leboncoin.data.db.AlbumsDatabase
import com.leboncoin.data.entities.AlbumsDataEntityMapper
import com.leboncoin.data.entities.AlbumsEntityDataMapper
import com.leboncoin.domain.entities.AlbumsSourcesEntity
import com.leboncoin.domain.entities.DataEntity
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.reactive.openSubscription

class AlbumsCacheImpl(private val database: AlbumsDatabase,
                      private val entityToDataMapper: AlbumsEntityDataMapper,
                      private val dataToEntityMapper: AlbumsDataEntityMapper
) : AlbumsDataStore {

    private val dao: AlbumsDao = database.getAlbumsDao()

    override suspend fun getAlbums(): ReceiveChannel<DataEntity<AlbumsSourcesEntity>> {
        val mappedValue = dao.getAllAlbums().map { list ->
            DataEntity.SUCCESS(dataToEntityMapper.mapToEntity(list))
        }
        return mappedValue.openSubscription()
    }

    fun saveAlbums(response: DataEntity<AlbumsSourcesEntity>) {
        entityToDataMapper.mapResponseToData(response)?.let {
            dao.clear()
            dao.saveAllAlbums(it)
        }
    }

}