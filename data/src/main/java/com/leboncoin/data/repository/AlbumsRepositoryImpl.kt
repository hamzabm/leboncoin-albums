package com.leboncoin.data.repository

import com.leboncoin.domain.entities.AlbumsSourcesEntity
import com.leboncoin.domain.entities.DataEntity
import com.leboncoin.domain.repository.AlbumsRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.*
import kotlinx.coroutines.launch

class AlbumsRepositoryImpl(private val remote: AlbumsRemoteImpl,
                           private val cache: AlbumsCacheImpl) : AlbumsRepository {

    override suspend fun getLocalAlbums(): ReceiveChannel<DataEntity<AlbumsSourcesEntity>> {
        return cache.getAlbums()
    }

    override suspend fun getRemoteAlbums(): ReceiveChannel<DataEntity<AlbumsSourcesEntity>> {
        return remote.getAlbums()
    }

    override suspend fun getAlbums(): ReceiveChannel<DataEntity<AlbumsSourcesEntity>> {
        val producerChannel = GlobalScope.produce() {

            launch {
                val localAlbumsChannel = cache.getAlbums()
                localAlbumsChannel.consumeEach { send(it) }
            }

            launch {
                val remoteAlbums = remote.getAlbums().receive()
                when (remoteAlbums) {
                    is DataEntity.SUCCESS -> {
                        cache.saveAlbums(remoteAlbums)
                    }
                    is DataEntity.ERROR -> {
                        send(remoteAlbums)
                    }
                }
            }
        }

        return producerChannel
    }
}