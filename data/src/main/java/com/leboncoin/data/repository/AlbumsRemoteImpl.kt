package com.leboncoin.data.repository

import com.leboncoin.data.api.RemoteAlbumsApi
import com.leboncoin.data.entities.AlbumsDataEntityMapper
import com.leboncoin.domain.entities.AlbumsSourcesEntity
import com.leboncoin.domain.entities.DataEntity
import com.leboncoin.domain.entities.ErrorEntity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.produce
import java.lang.Exception

class AlbumsRemoteImpl constructor(private val api: RemoteAlbumsApi) : AlbumsDataStore {

    private val AlbumsMapper = AlbumsDataEntityMapper()

    override suspend fun getAlbums(): ReceiveChannel<DataEntity<AlbumsSourcesEntity>> {
        val producer = GlobalScope.produce<DataEntity<AlbumsSourcesEntity>> {
            try {
                val Albums = api.getAlbums().await()
                AlbumsMapper.mapToEntity(Albums).let { send(DataEntity.SUCCESS(it)) }
            } catch (e: Exception) {
                send(DataEntity.ERROR(ErrorEntity(e.message)))
            }
        }

        return producer
    }

}