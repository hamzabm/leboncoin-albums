package com.leboncoin.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "albums")
data class AlbumData(
        @PrimaryKey(autoGenerate = false) var id: Int,
        var albumId: Int,
        var thumbnailUrl: String,
        var title: String,
        var url: String)