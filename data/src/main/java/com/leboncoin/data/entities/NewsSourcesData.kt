package com.leboncoin.data.entities

import com.leboncoin.domain.entities.AlbumsEntity
import com.leboncoin.domain.entities.AlbumsSourcesEntity
import com.leboncoin.domain.entities.DataEntity


class AlbumsDataEntityMapper constructor() {

    fun mapToEntity(mapAlbums: List<AlbumData>?) =
            AlbumsSourcesEntity(albums = mapListAlbumsToEntity(mapAlbums))


    fun mapListAlbumsToEntity(albums: List<AlbumData>?)
            : List<AlbumsEntity> = albums?.map { mapAlbumToEntity(it) } ?: emptyList()

    fun mapAlbumToEntity(response: AlbumData): AlbumsEntity = AlbumsEntity(
            id = response.id,
            thumbnailUrl = response.thumbnailUrl,
            albumId = response.albumId,
            url = response.url,
            title = response.title
    )


}


class AlbumsEntityDataMapper constructor() {

    fun mapAlbumToData(response: AlbumsEntity): AlbumData = AlbumData(
            id = response.id,
            thumbnailUrl = response.thumbnailUrl,
            albumId = response.albumId,
            url = response.url,
            title = response.title
    )

    fun mapResponseToData(response: DataEntity<AlbumsSourcesEntity>): List<AlbumData>? {
        when (response) {
            is DataEntity.SUCCESS<AlbumsSourcesEntity> ->
                return@mapResponseToData response.data?.albums?.map { mapAlbumToData(it) }
            is DataEntity.ERROR<AlbumsSourcesEntity> ->
                return@mapResponseToData response.data?.albums?.map { mapAlbumToData(it) }
            is DataEntity.LOADING<AlbumsSourcesEntity> ->
                return@mapResponseToData response.data?.albums?.map { mapAlbumToData(it) }
        }
    }



}

